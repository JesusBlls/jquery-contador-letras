$(document).ready(function() {
    $('#texto_visitante').on('keyup', function() {

        var totalDeLetras = $('#texto_visitante').val().length;
        var totalDePalabras = $('#texto_visitante').val().split(' ').length - 1;

        $('#texto_contador').text('Total de letras: ' + totalDeLetras + ' Total de Palabras: ' + totalDePalabras);
    })
});
